FROM python:3.8.5

RUN apt-get update -y && apt-get install -y --no-install-recommends \
    python-dev \
    python-setuptools && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /home/ubuntu/projects/bitcoin
ENV PYTHONDONTWRITEBYTECODE=1 PYTHONUNBUFFERED=1
ENV PYTHONBREAKPOINT=ipdb.set_trace

RUN pip install --upgrade pip

COPY requirements.txt /tmp/requirements.txt
COPY requirements.dev.txt /tmp/requirements.dev.txt

RUN pip install --no-cache-dir -r /tmp/requirements.dev.txt

CMD ["gunicorn", "-w", "4", "-b", "0.0.0.0:8000", "--chdir", "/home/ubuntu/projects/bitcoin/src", "settings.wsgi", "--timeout", "30", "--log-level", "error", "--max-requests", "10000"]
