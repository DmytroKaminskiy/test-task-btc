from decorators import lock_for_user

from django.db import transaction
from django.db.models import Q, Sum
from django.utils.decorators import method_decorator

from rest_framework import generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from wallet import choices, serializers
from wallet.models import Transaction, Wallet


class WalletsView(generics.ListCreateAPIView):
    serializer_class = serializers.WalletSerializer

    def get_queryset(self):
        return self.request.user.wallet_set.all().order_by('address')

    @method_decorator(lock_for_user(methods=('POST',)))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class WalletView(generics.RetrieveAPIView):
    serializer_class = serializers.WalletSerializer

    def get_queryset(self):
        return self.request.user.wallet_set.all()


class TransactionsView(generics.ListCreateAPIView):
    queryset = Transaction.objects.all().order_by('-created')
    serializer_class = serializers.CreateTransactionSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        wallets = tuple(self.request.user.wallet_set.values_list('address', flat=True))
        return queryset.filter(Q(from_wallet__in=wallets) | Q(to_wallet__in=wallets))

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return serializers.TransactionSerializer
        return super().get_serializer_class()

    def create(self, request, *args, **kwargs):
        with transaction.atomic():

            # lock record if needed
            get_object_or_404(Wallet.objects.select_for_update(), pk=request.data.get('from_wallet', ''),
                              user=request.user)
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=False)
            if serializer.errors:
                response = Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                self.perform_create(serializer)
                headers = self.get_success_headers(serializer.data)
                response = Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

        for func in serializer.post_transaction_funcs:
            func()

        return response


class WalletTransactionsView(generics.ListAPIView):
    serializer_class = serializers.TransactionSerializer
    queryset = Transaction.objects.all().order_by('-id')

    def get_queryset(self):
        queryset = super().get_queryset()
        address = self.kwargs['address']
        return queryset.filter(Q(from_wallet_id=address) | Q(to_wallet_id=address))


class StatisticsView(generics.GenericAPIView):

    def get(self, request):
        total = Transaction.objects.count()
        profit = Transaction.objects\
            .filter(status=choices.STATUS_SUCCESS, org_profit__isnull=False)\
            .aggregate(profit=Sum('org_profit'))['profit'] or 0

        data = {
            'results': {
                'total': total,
                'profit': profit,
            },
        }
        return Response(data, status=status.HTTP_200_OK)
