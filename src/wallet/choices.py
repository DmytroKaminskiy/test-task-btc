
STATUS_SUCCESS, STATUS_INSUFFICIENT_FUNDS = 200, 400

STATUSES = (
    (STATUS_SUCCESS, 'success'),
    (STATUS_INSUFFICIENT_FUNDS, 'insufficient funds'),
)
