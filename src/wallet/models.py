from bitcoin import privtopub, pubtoaddr, random_key

from constance import config

from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator
from django.db import DatabaseError, models, transaction
from django.db.models import F

from rate.models import Rate
from rate.utils import to_decimal

from wallet import choices


BTC_SATOSHI = 100_000_000


class Wallet(models.Model):

    address = models.CharField(max_length=128, primary_key=True, editable=False)
    public_key = models.CharField(max_length=130, editable=False)
    private_key = models.CharField(max_length=128, default=random_key, editable=False)

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    satoshi = models.BigIntegerField(default=BTC_SATOSHI, validators=[MinValueValidator(0)])

    @property
    def btc(self):
        return to_decimal(self.satoshi / BTC_SATOSHI, 10)

    @property
    def usd(self):
        rate = Rate.get_solo()
        return self.btc * rate.amount

    def save(self, *args, **kwargs):

        if self._state.adding:
            if self.__class__.objects.filter(user_id=self.user_id).count() >= config.USER_MAX_WALLETS:
                raise DatabaseError(f'Max wallets number: {config.USER_MAX_WALLETS}')

            # generate wallet address
            self.public_key = privtopub(self.private_key)
            self.address = pubtoaddr(self.public_key)

        super().save(*args, **kwargs)


class Transaction(models.Model):
    from_wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE, related_name='outs')
    to_wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE, related_name='ins')
    created = models.DateTimeField(auto_now_add=True)
    initial_amount = models.BigIntegerField(help_text='amount in satoshi', validators=[MinValueValidator(1)])
    final_amount = models.BigIntegerField(help_text='amount in satoshi', null=True)
    org_profit = models.BigIntegerField(null=True, blank=True, help_text='amount in satoshi')
    status = models.PositiveSmallIntegerField(choices=choices.STATUSES)

    @classmethod
    def create(cls, validated_data):
        validated_data['status'] = choices.STATUS_SUCCESS

        if validated_data['from_wallet'].user == validated_data['to_wallet'].user:
            validated_data['final_amount'] = validated_data['initial_amount']
        else:
            validated_data['final_amount'] = int(validated_data['initial_amount'] * (1 - config.TRANSACTION_COMMISSION))
            validated_data['org_profit'] = validated_data['initial_amount'] - validated_data['final_amount']

        with transaction.atomic():
            instance = Transaction.objects.create(**validated_data)

            Wallet.objects \
                .filter(pk=validated_data['from_wallet'].pk) \
                .update(satoshi=F('satoshi') - validated_data['initial_amount'])
            Wallet.objects \
                .filter(pk=validated_data['to_wallet'].pk) \
                .update(satoshi=F('satoshi') + validated_data['final_amount'])

        return instance
