from constance import config

from rest_framework import serializers
from rest_framework.fields import empty

from wallet import choices as mch
from wallet.models import Transaction, Wallet


class WalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wallet
        fields = ('address', 'satoshi', 'usd', 'btc')

        extra_kwargs = {
            'satoshi': {'read_only': True},
            'usd': {'read_only': True},
            'btc': {'read_only': True},
        }

    def __init__(self, instance=None, data=empty, *args, **kwargs):
        super().__init__(instance, data, *args, **kwargs)
        self.request = kwargs['context']['request']

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if self.request.user.wallet_set.count() >= config.USER_MAX_WALLETS:
            raise serializers.ValidationError(f'Max wallets number: {config.USER_MAX_WALLETS}')
        return attrs


class CreateTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('from_wallet', 'to_wallet', 'initial_amount')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.post_transaction_funcs = []

    def validate(self, attrs):
        attrs = super().validate(attrs)

        if attrs['from_wallet'] == attrs['to_wallet']:
            raise serializers.ValidationError('Cannot create transaction on the same wallet.')

        if attrs['from_wallet'].satoshi < attrs['initial_amount']:
            self.post_transaction_funcs.append(
                lambda: Transaction.objects.create(status=mch.STATUS_INSUFFICIENT_FUNDS, **attrs),
            )
            raise serializers.ValidationError('Insufficient Funds.', code='insufficient_funds')

        return attrs

    def create(self, validated_data):
        return Transaction.create(validated_data)


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = (
            'initial_amount',
            'to_wallet_id',
            'from_wallet_id',
            'created',
            'initial_amount',
            'final_amount',
            'org_profit',
            'status',
            'get_status_display',
        )
