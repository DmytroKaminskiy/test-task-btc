from django.urls import path, re_path

from wallet import views

app_name = 'wallet'

urlpatterns = [
    path('', views.WalletsView.as_view(), name='wallets'),
    re_path(r'^(?P<pk>\w{30,40})/$', views.WalletView.as_view(), name='wallet'),
    re_path(r'^wallets/(?P<address>\w{30,40})/transactions/$', views.WalletTransactionsView.as_view(),
            name='wallet-transactions'),
    path('transactions/', views.TransactionsView.as_view(), name='transactions'),
    path('statistics/', views.StatisticsView.as_view(), name='statistics'),
]
