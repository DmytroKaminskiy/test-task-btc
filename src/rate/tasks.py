from celery import shared_task

import requests


@shared_task(time_limit=60)
def parse():
    from rate.models import Rate

    url = 'https://api.coindesk.com/v1/bpi/currentprice.json'
    response = requests.get(url)
    response.raise_for_status()

    response_json = response.json()
    time_ = response_json['time']['updatedISO']

    rate = Rate.get_solo()

    if rate.updated != time_:
        rate.updated = time_
        rate.amount = response_json['bpi']['USD']['rate_float']
        rate.save()
