from django.db import models

from solo.models import SingletonModel


class Rate(SingletonModel):
    amount = models.DecimalField(max_digits=10, decimal_places=4, default=0)
    updated = models.CharField(max_length=64)
