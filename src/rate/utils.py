from decimal import Decimal


def to_decimal(num, prec: int = 4) -> Decimal:
    return Decimal(round(num, prec))
