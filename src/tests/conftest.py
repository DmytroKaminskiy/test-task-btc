import os

from django.conf import settings
from django.core.management import call_command

from faker import Faker

import pytest

from pytest_django.fixtures import _django_db_fixture_helper

from pytest_factoryboy import register

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from tests.factories import *  # noqa


register(UserFactory)  # noqa
register(WalletFactory)  # noqa


@pytest.fixture(scope='session', autouse=True)
def _db_session(request, django_db_setup, django_db_blocker):
    """
    Changed scope to 'session'
    """
    if 'django_db_reset_sequences' in request.funcargnames:
        request.getfixturevalue('django_db_reset_sequences')
    if 'transactional_db' in request.funcargnames \
            or 'live_server' in request.funcargnames:
        request.getfixturevalue('transactional_db')
    else:
        _django_db_fixture_helper(request, django_db_blocker, transactional=False)


@pytest.fixture(scope='session', autouse=True)
def _fixtures():
    call_command('loaddata', os.path.join(settings.BASE_DIR, 'tests', 'fixtures', 'rates.json'))


@pytest.fixture(scope='session')
def api_client():
    return APIClient()


@pytest.fixture(scope='session')
def api_client_auth(fake):
    client = APIClient()

    data = {
        'email': fake.email(),
        'password': fake.password(),
    }

    response = client.post(
        reverse('account:users'),
        data=data,
    )

    assert response.status_code == status.HTTP_201_CREATED, response.content
    assert "access" in response.json(), response.content
    token = response.json()['access']
    client.credentials(
        HTTP_AUTHORIZATION=f'JWT {token}',
    )

    return client


@pytest.fixture(scope="session")
def fake():
    return Faker()


@pytest.fixture(autouse=True)
def requests_get_mock(mocker):
    return mocker.patch('requests.get')
