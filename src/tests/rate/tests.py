from unittest.mock import MagicMock

from rate.models import Rate
from rate.tasks import parse

from rest_framework import status


TEST_RESPONSE = {
    "time": {
        "updated": "Aug 6, 2020 09:05:00 UTC",
        "updatedISO": "2020-08-06T09:05:00+00:00",
        "updateduk": "Aug 6, 2020 at 10:05 BST",
    },
    "disclaimer": "This data was produced from the CoinDesk Bitcoin Price Index (USD). "
                  "Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
    "chartName": "Bitcoin",
    "bpi": {
        "USD": {
            "code": "USD",
            "symbol": "&#36;",
            "rate": "11,722.6999",
            "description": "United States Dollar",
            "rate_float": 11722.6999,
        },
        "GBP": {
            "code": "GBP",
            "symbol": "&pound;",
            "rate": "8,956.7757",
            "description": "British Pound Sterling",
            "rate_float": 8956.7757,
        },
        "EUR": {
            "code": "EUR",
            "symbol": "&euro;",
            "rate": "9,917.7323",
            "description": "Euro",
            "rate_float": 9917.7323,
        },
    },
}


def test_parse_rate(requests_get_mock, socket_enabled):
    def mock():
        response = MagicMock()
        response.json = lambda: TEST_RESPONSE
        response.status_code = status.HTTP_200_OK
        return response

    requests_get_mock.return_value = mock()
    parse()
    assert Rate.objects.count() == 1
    parse()
    assert Rate.objects.count() == 1

    # test rate save()
    rate = Rate.objects.last()
    rate.save()
