from constance import config

from django.db import DatabaseError

import pytest

from rest_framework import status
from rest_framework.reverse import reverse

from tests.factories import UserFactory, WalletFactory


@pytest.mark.enable_socket()
def test_wallets(api_client_auth):
    url = reverse('wallet:wallets')
    response = api_client_auth.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['results']

    address = response.json()['results'][0]['address']
    url = reverse('wallet:wallet', args=(address, ))
    response = api_client_auth.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert response.json()


@pytest.mark.enable_socket()
def test_wallet_create(api_client_auth):
    url = reverse('wallet:wallets')
    response = api_client_auth.post(url)

    assert response.status_code == status.HTTP_201_CREATED
    assert response.json()

    for _ in range(config.USER_MAX_WALLETS):
        response = api_client_auth.post(url)
        if response.status_code != status.HTTP_201_CREATED:
            break

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response_json = response.json()
    assert response_json == {'non_field_errors': ['Max wallets number: 10']}

    user = response.wsgi_request.user

    addresses = user.wallet_set.all()[1:].values_list('address', flat=True)

    # test wallet save
    wallet = user.wallet_set.last()
    addr = wallet.address
    wallet.save()
    assert wallet.address == addr

    # cleanup
    user.wallet_set.filter(address__in=addresses).delete()


def test_statistics(api_client_auth):
    url = reverse('wallet:statistics')
    response = api_client_auth.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['results']
    assert 'total' in response.json()['results']
    assert 'profit' in response.json()['results']


def test_wallet_create_save_method():
    user = UserFactory()

    with pytest.raises(DatabaseError) as exc:  # noqa
        for _ in range(config.USER_MAX_WALLETS + 1):
            WalletFactory(user=user)

    user.wallet_set.all().delete()
    assert exc.value.args == (f'Max wallets number: {config.USER_MAX_WALLETS}',)
