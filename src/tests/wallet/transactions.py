from constance import config

from rest_framework import status
from rest_framework.reverse import reverse

from tests.factories import WalletFactory

from wallet import choices as mch
from wallet.models import Transaction


def test_get_transactions(api_client_auth):
    url = reverse('wallet:transactions')
    response = api_client_auth.get(url)
    assert response.status_code == status.HTTP_200_OK


def test_create_transaction(api_client_auth, wallet):
    initial_transaction_count = Transaction.objects.count()

    url = reverse('wallet:transactions')
    response = api_client_auth.post(url)
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert response.json() == {'detail': 'Not found.'}
    assert Transaction.objects.count() == initial_transaction_count

    user = response.wsgi_request.user

    wallet1 = user.wallet_set.first()
    wallet2 = WalletFactory(user=user)

    invalid_pk = f'{"1"*8}-{"1"*4}-{"1"*4}-{"1"*4}-{"1"*12}'

    data = {
        'from_wallet': invalid_pk,
        'to_wallet': wallet2.address,
        'initial_amount': 100,
    }
    response = api_client_auth.post(url, data=data)
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert response.json() == {'detail': 'Not found.'}
    assert Transaction.objects.count() == initial_transaction_count

    data['from_wallet'] = wallet2.address
    response = api_client_auth.post(url, data=data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {'non_field_errors': ['Cannot create transaction on the same wallet.']}
    assert Transaction.objects.count() == initial_transaction_count

    data['from_wallet'] = wallet1.address
    data['initial_amount'] = 92233720368547758
    response = api_client_auth.post(url, data=data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert Transaction.objects.count() == initial_transaction_count + 1
    transaction = Transaction.objects.last()
    assert transaction.status == mch.STATUS_INSUFFICIENT_FUNDS

    data['initial_amount'] = 100_000
    response = api_client_auth.post(url, data=data)
    assert response.status_code == status.HTTP_201_CREATED
    assert Transaction.objects.count() == initial_transaction_count + 2
    transaction = Transaction.objects.last()
    assert transaction.status == mch.STATUS_SUCCESS
    assert transaction.initial_amount == data['initial_amount']
    assert transaction.final_amount == data['initial_amount']
    assert transaction.org_profit is None

    initial_value = wallet1.satoshi
    wallet1.refresh_from_db()
    wallet2.refresh_from_db()

    assert wallet1.satoshi == initial_value - data['initial_amount']
    assert wallet2.satoshi == initial_value + data['initial_amount']

    data['to_wallet'] = wallet.pk
    response = api_client_auth.post(url, data=data)
    assert response.status_code == status.HTTP_201_CREATED
    assert Transaction.objects.count() == initial_transaction_count + 3
    transaction = Transaction.objects.last()
    assert transaction.status == mch.STATUS_SUCCESS
    assert transaction.initial_amount == data['initial_amount']
    assert transaction.final_amount == int(data['initial_amount'] * (1 - config.TRANSACTION_COMMISSION))
    assert transaction.org_profit == int(data['initial_amount'] * config.TRANSACTION_COMMISSION)

    initial_value1 = wallet1.satoshi
    initial_value = wallet.satoshi

    wallet1.refresh_from_db()
    wallet.refresh_from_db()

    assert wallet1.satoshi == initial_value1 - data['initial_amount']
    assert wallet.satoshi == initial_value + int(data['initial_amount'] * (1 - config.TRANSACTION_COMMISSION))


def test_wallet_transactions(api_client_auth):
    url = reverse('wallet:wallets')
    response = api_client_auth.post(url, data={})
    assert response.status_code == status.HTTP_201_CREATED
    wallet = response.json()['address']

    url = reverse('wallet:wallet-transactions', args=(wallet, ))
    response = api_client_auth.get(url)
    assert response.status_code == status.HTTP_200_OK
