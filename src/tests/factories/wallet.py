import factory

from tests.factories.user import UserFactory

from wallet.models import Wallet


class WalletFactory(factory.DjangoModelFactory):
    class Meta:
        model = Wallet

    user = factory.SubFactory(UserFactory)
