SOLO_CACHE = 'default'
SOLO_CACHE_TIMEOUT = 60 * 60  # 60 mins
SOLO_CACHE_PREFIX = 'django-solo'
