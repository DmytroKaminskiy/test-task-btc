CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'

CONSTANCE_CONFIG = {
    'USER_MAX_WALLETS': (10, 'Max Wallets allowed to be created.'),
    'TRANSACTION_COMMISSION': (0.0015, 'Profit percent from transaction.'),
}
