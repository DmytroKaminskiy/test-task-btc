SHELL := /bin/bash

run_backend := docker-compose run --rm backend
manage_py := $(run_backend) python ./src/manage.py

help:
	@sed -ne '/@sed/!s/## //p' $(MAKEFILE_LIST)

logs_backend:  ## display switcher container logs
	docker logs -f --tail 1000 backend

shell:  ## run python shell using switcher container
	$(manage_py) shell_plus --print-sql

migrate:  ## run django migrations
	$(manage_py) migrate

makemigrations:  ## create django migrations
	$(manage_py) makemigrations

build:  ## build project using docker-compose
	cp -n .env.example .env && \
	docker-compose up -d --build && \
	sleep 3 && \
	docker exec -it backend python ./src/manage.py migrate

flake8:  ## run flake8 in app directory
	$(run_backend) flake8 src

pytest:  ## run pytest in app/tests
	$(run_backend) pytest src --cov=src --cov-report html -vvv

show-coverage:  ## open coverage HTML report in default browser
	python3 -c "import webbrowser; webbrowser.open('.pytest_cache/coverage/index.html')"

check: flake8 pytest  ## run all available checks [flake8, pytest...]
