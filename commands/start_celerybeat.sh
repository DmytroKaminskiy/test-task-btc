#!/bin/bash

SCHEDULE=/tmp/celerybeat-schedule
PID_FILE=/tmp/celerybeat.pid

rm ${SCHEDULE} ${PID_FILE}

celery -A settings beat --loglevel=info \
       --workdir=${PROJECT_ROOT}/src \
       --schedule=${SCHEDULE} \
       --pidfile=${PID_FILE}
