#!/bin/bash

PID_FILE=/tmp/celery.pid

rm ${PID_FILE}

celery -A settings worker -E --loglevel=info --workdir=${PROJECT_ROOT}/src --pidfile=${PID_FILE}
